import java.io.*;
import java.util.Map;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import javax.print.DocFlavor;


public class DBCreator {


    @VisibleForTesting
    static boolean checkIfDBExists(String dbName) {
        Preconditions.checkNotNull(dbName);

        File dir = new File(EngineConstants.DB_PATH);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (FilenameUtils.removeExtension(child.getName()).equals(dbName )) {
                    return true;
                }
            }
        }

        Preconditions.checkNotNull(dbName);

        return false;
    }

    public static void createDataBase(String dbName) throws DBAlreadyExistsException {
        Preconditions.checkNotNull(dbName);

        if (checkIfDBExists(dbName)) {
            throw new DBAlreadyExistsException();
        }

        String dbContent = String.format(EngineConstants.DB_TEMPLATE,  dbName);
        try {
            FileUtils.writeStringToFile(new File(EngineConstants.DB_PATH + "/" + dbName + ".xml"), dbContent);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Preconditions.checkNotNull(dbName);
    }
}
