import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class TableManager {

    public static void createTable(String database, String tableName, Map<String, String> tableFields)
            throws IOException {

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(tableFields);

        // create fields
        String filedsDefString = "";
        for (Map.Entry<String, String> entry : tableFields.entrySet()) {

            filedsDefString += String.format(EngineConstants.FIELD_DEFINITION_TEMPLATE, entry.getKey(),
                    entry.getValue());
        }

        String tableFieldsDefString = String.format(EngineConstants.TABLE_FILED_DEF_TEMPLATE, filedsDefString);
        String tableDef = String.format(EngineConstants.TABLE_TEMPLATE, tableName, tableFieldsDefString);
        BufferedWriter out = null;



        String dbContent =  FileUtils.readFileToString(new File(EngineConstants.DB_PATH + "/" + database),
                "UTF-8");

        File file = new File(EngineConstants.DB_PATH + "/" + database);
        FileUtils.writeStringToFile(file, insertNewTable(dbContent, tableDef));


        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(tableFields);
    }

    @VisibleForTesting
    static String insertNewTable(String rawDB, String newTable) {
        Preconditions.checkNotNull(rawDB);
        Preconditions.checkNotNull(newTable);

        int insertIndex = rawDB.indexOf("</db>");

        Preconditions.checkNotNull(rawDB.substring(0, insertIndex) + newTable + rawDB.substring(insertIndex, rawDB.length()));
        Preconditions.checkNotNull(rawDB);
        Preconditions.checkNotNull(newTable);

        return rawDB.substring(0, insertIndex) + newTable + rawDB.substring(insertIndex, rawDB.length());
    }
}
