import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExportsManager {

    public static void generateExport(String database) throws IOException {

        Preconditions.checkNotNull(database);

        FileUtils.writeStringToFile(new File(EngineConstants.EXPORT_PATH + "/export.csv"), "");

        String dbContent = FileUtils.readFileToString(new File(EngineConstants.DB_PATH + "/" + database),
                "UTF-8");

        String tableFinderRegex = "<table name=\"\\w+\">";
        Pattern pattern = Pattern.compile(tableFinderRegex);
        Matcher matcher = pattern.matcher(dbContent);

        while (matcher.find()) {
            String tableRaw = matcher.group(0);

            int index1 = tableRaw.indexOf("\"");
            tableRaw = tableRaw.substring(index1 + 1, tableRaw.length());

            int index2 = tableRaw.indexOf("\"");

            String tableName = tableRaw.substring(0, index2);

            List<String> entries = SelectManager.selectEntries(database, tableName);

            FileUtils.writeStringToFile(new File(EngineConstants.EXPORT_PATH + "/export.csv"),
                    tableName + "\n", "UTF-8");

            for (String entry : entries) {
                FileUtils.writeStringToFile(new File(EngineConstants.EXPORT_PATH + "/export.csv"),
                        entry, "UTF-8");
            }

        }

        Preconditions.checkNotNull(database);
    }
}
