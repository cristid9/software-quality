import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DropManager {

    public static void dropTable(String database, String tableName) throws IOException {
        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);

        String dbContent = FileUtils.readFileToString(new File(EngineConstants.DB_PATH + "/" + database),
                "UTF-8");

        int startIndex = dbContent.indexOf(String.format("<table name=\"%s\"", tableName));
        String workingString = dbContent.substring(startIndex, dbContent.length());

        int endIndex = workingString.indexOf("</table>");

        workingString = workingString.substring(0, endIndex);

        dbContent = dbContent.substring(0, startIndex) + dbContent.substring(startIndex + workingString.length(), dbContent.length());

        File file = new File(EngineConstants.DB_PATH + "/" + database);
        FileUtils.writeStringToFile(file, dbContent);

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
    }
}
