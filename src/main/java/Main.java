import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Welcom to my_db, enter commands below, press ctrl + c to exist");
        CommandsDispatcher commandsDispatcher = new CommandsDispatcher();
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print(">>> ");
            String line = sc.nextLine();
            commandsDispatcher.dispatchCommand(line);

        }
    }
}
