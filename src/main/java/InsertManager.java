import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class InsertManager {


    public static void insertIntoTable(String database, String tableName, List<String> defArgs,
                                       List<String> insertArgs) throws IOException {

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(defArgs);
        Preconditions.checkNotNull(insertArgs);

        String fileds = "";

        for (int i = 0; i < defArgs.size(); ++i) {
            fileds += String.format(EngineConstants.INSERTED_FIELD_TEMPLATE, defArgs.get(i), insertArgs.get(i));
        }

        String dbContent = FileUtils.readFileToString(new File(EngineConstants.DB_PATH + "/" + database),
                "UTF-8");
        fileds = String.format(EngineConstants.ENTRY_TEMPLATE, fileds);

        File file = new File(EngineConstants.DB_PATH + "/" + database);
        FileUtils.writeStringToFile(file, internalInsertTableEntries(dbContent, tableName, fileds));

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(defArgs);
        Preconditions.checkNotNull(insertArgs);
    }

    @VisibleForTesting
    static String internalInsertTableEntries(String dbContent, String tableName, String fileds) {

        Preconditions.checkNotNull(dbContent);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(fileds);

        String tableTargetToken = String.format("<table name=\"%s\"", tableName);
        int tableIndex = dbContent.indexOf(tableTargetToken);

        String targetString = dbContent.substring(tableIndex, dbContent.length());

        int insertIndex = targetString.indexOf("</entries>");

        targetString = targetString.substring(0, insertIndex) + fileds
                + targetString.substring(insertIndex, targetString.length());

        String updatedDBContent = dbContent.substring(0, tableIndex) + targetString;

        Preconditions.checkNotNull(dbContent);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(fileds);

        return updatedDBContent;
    }

}
