import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DeleteManager {

    public static void deleteEntry(String database, String tableName, String fieldName, String fieldValue) throws IOException {

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(fieldName);
        Preconditions.checkNotNull(fieldValue);

        String dbContent = FileUtils.readFileToString(new File(EngineConstants.DB_PATH + "/" + database),
                "UTF-8");

        String fieldXML = String.format(EngineConstants.INSERTED_FIELD_TEMPLATE, fieldName, fieldValue);

        File file = new File(EngineConstants.DB_PATH + "/" + database);
        FileUtils.writeStringToFile(file, internalDeleteEntry(dbContent, fieldXML));

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(fieldName);
        Preconditions.checkNotNull(fieldValue);
    }

    @VisibleForTesting
    static String internalDeleteEntry(String dbContent, String filedXML) {
        Preconditions.checkNotNull(dbContent);
        Preconditions.checkNotNull(filedXML);

        int deleteIndex = dbContent.indexOf(filedXML);
        dbContent = dbContent.substring(0, deleteIndex)
                + dbContent.substring(deleteIndex + filedXML.length(), dbContent.length());

        Preconditions.checkNotNull(dbContent);
        Preconditions.checkNotNull(filedXML);

        return dbContent;
    }
}
