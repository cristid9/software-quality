import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SelectManager {

    public static List<String> selectEntries(String database, String tableName) throws IOException {

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);

        String dbContent = FileUtils.readFileToString(new File(EngineConstants.DB_PATH + "/" + database),
                "UTF-8");

        int startIndex = dbContent.indexOf(String.format("<table name=\"%s\"", tableName));

        String workingContent = dbContent.substring(startIndex, dbContent.length());

        int entriesStartIndex = workingContent.indexOf("<entries>");

        if (entriesStartIndex == -1) return null;

        int entriesEndIndex = workingContent.indexOf("</entries>");

        String entriesRaw = workingContent.substring(entriesStartIndex, entriesEndIndex);

        String entryRegex = "<entry>.*?</entry>";
        Pattern pattern = Pattern.compile(entryRegex);
        Matcher matcher = pattern.matcher(entriesRaw);

        List<String> selectResult = new ArrayList<String>();

        while (matcher.find()) {
            String rawEntry = matcher.group(0);
            String entries = "";

            String fieldRegexp = String.format(EngineConstants.INSERTED_FIELD_TEMPLATE, "[a-z0-9 ]+", "[a-z0-9 ]+");
            Pattern pattern1 = Pattern.compile(fieldRegexp);
            Matcher matcher1 = pattern1.matcher(rawEntry);

            while (matcher1.find()) {
                String filedRaw = matcher1.group(0);

                int firstIndex = filedRaw.indexOf("\"");
                filedRaw = filedRaw.substring(firstIndex + 1, filedRaw.length());

                int index2 = filedRaw.indexOf("\"");
                filedRaw = filedRaw.substring(index2 + 1, filedRaw.length());

                int index3 = filedRaw.indexOf("\"");
                filedRaw = filedRaw.substring(index3 + 1, filedRaw.length());

                int index4 = filedRaw.indexOf("\"");

                entries += filedRaw.substring(0, index4) + ", ";

            }
            entries += "\n";

            selectResult.add(entries);
        }

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);

        return selectResult;
    }

}
