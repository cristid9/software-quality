import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class UpdateManager {


    public static void updateEntry(String database, String tableName,
                                   String fieldName, String oldValue, String newValue) throws IOException {

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(fieldName);
        Preconditions.checkNotNull(oldValue);
        Preconditions.checkNotNull(newValue);

        String dbContent = FileUtils.readFileToString(new File(EngineConstants.DB_PATH + "/" + database),
                "UTF-8");

        String newEntry = String.format(EngineConstants.INSERTED_FIELD_TEMPLATE, fieldName, newValue);
        String oldEntry = String.format(EngineConstants.INSERTED_FIELD_TEMPLATE, fieldName, oldValue);

        File file = new File(EngineConstants.DB_PATH + "/" + database);
        FileUtils.writeStringToFile(file, internalUpdateEntry(dbContent, oldEntry, newEntry));

        Preconditions.checkNotNull(database);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(fieldName);
        Preconditions.checkNotNull(oldValue);
        Preconditions.checkNotNull(newValue);
    }

    @VisibleForTesting
    static String internalUpdateEntry(String dbContent, String oldEntry, String newEntry) {
        Preconditions.checkNotNull(dbContent);
        Preconditions.checkNotNull(oldEntry);
        Preconditions.checkNotNull(newEntry);

        int insertIndex = dbContent.indexOf(oldEntry);

        dbContent = dbContent.substring(0, insertIndex) + newEntry
                + dbContent.substring(insertIndex + oldEntry.length(), dbContent.length());

        Preconditions.checkNotNull(dbContent);
        Preconditions.checkNotNull(oldEntry);
        Preconditions.checkNotNull(newEntry);

        return dbContent;
    }
}
