public class EngineConstants {

    public static final String DB_PATH = "/home/cristi/db_mg_sys/dbs";
    public static final String DB_TEMPLATE = "<db name=\"%s\">" +
            "</db>";
    public static final String TABLE_TEMPLATE = "<table name=\"%s\">%s<entries></entries></table>";
    public static final String FIELD_DEFINITION_TEMPLATE = "<fieldDef name=\"%s\" value=\"%s\"/>";
    public static final String TABLE_FILED_DEF_TEMPLATE = "<fieldsDef> %s </fieldsDef>";
    public static final String CREATE_DB_COMMAND = "CREATE";
    public static final String MAKE_COMMAND = "MAKE";
    public static final String OPEN_COMMAND = "OPEN";
    public static final String INSERT_COMMAND = "INSERT";
    public static final String ENTRY_TEMPLATE = "<entry> %s </entry>";
    public static final String INSERTED_FIELD_TEMPLATE = "<field name=\"%s\" value=\"%s\"/>";
    public static final String UPDATE_COMMAND = "UPDATE";
    public static final String DELETE_COMMAND = "DELETE";
    public static final String DROP_COMMAND = "DROP";
    public static final String SELECT_COMMAND = "SELECT";
    public static final String EXPORT_PATH = "/home/cristi/db_mg_sys/exports";
    public static final String EXORT_COMMAND = "EXPORT";
    public static final String IMPORT_COMMAND = "IMPORT";
}
