import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * We assume we already know the syntax for each command in this class.
 */
public class CommandsDispatcher {

    private String currentDb = "ana.xml";

    public CommandsDispatcher() {}

    public void dispatchCommand(String line) {
        Preconditions.checkNotNull(line);

        int index = line.indexOf(" ");
        String command = line.substring(0, index).toUpperCase();

        if (command.equals(EngineConstants.CREATE_DB_COMMAND)) {
            dispatchCreate(line);
        } else if (command.equals(EngineConstants.MAKE_COMMAND)) {
            dispatchMake(line);
        } else if (command.equals(EngineConstants.OPEN_COMMAND)) {
            dispatchOpen(line);
        } else if (command.equals(EngineConstants.INSERT_COMMAND)) {
            dispatchInsert(line);
        } else if (command.equals(EngineConstants.UPDATE_COMMAND)) {
            dispatchUpdate(line);
        } else if (command.equals(EngineConstants.DELETE_COMMAND)) {
            dispatchDelete(line);
        } else if (command.equals(EngineConstants.SELECT_COMMAND)) {
            dispatchSelect(line);
        } else if (command.equals(EngineConstants.DROP_COMMAND)) {
            dispatchDrop(line);
        } else if (command.equals(EngineConstants.EXORT_COMMAND)) {
            dispatchExport(line);
        } else if (command.equals(EngineConstants.IMPORT_COMMAND)) {
            dispatchImport(line);
        }

        Preconditions.checkNotNull(line);
    }

    @VisibleForTesting
    void dispatchImport(String line) {
        ImportManager.handleImport();
    }

    @VisibleForTesting
    void dispatchExport(String line) {
        try {
            ExportsManager.generateExport(this.currentDb);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @VisibleForTesting
    void dispatchDrop(String line) {
        Preconditions.checkNotNull(line);
        String targetTable = line.split(" ")[1];
        try {
            DropManager.dropTable(this.currentDb, targetTable);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Preconditions.checkNotNull(line);
    }

    @VisibleForTesting
    void dispatchSelect(String line) {
        String targetTable = line.split(" ")[3];
        Preconditions.checkNotNull(line);

        try {
            List<String> entries = SelectManager.selectEntries(this.currentDb, targetTable);

            for (String entry : entries) {
                System.out.print(entry);
            }

            System.out.println(" ");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Preconditions.checkNotNull(line);
    }

    @VisibleForTesting
    void dispatchDelete(String line) {
        Preconditions.checkNotNull(line);

        String[] tokens = line.split(" ");

        String targetTable = tokens[3];
        String fieldName = tokens[1];

        String deleteRegexp = "\\w+=\\w+";
        Pattern pattern = Pattern.compile(deleteRegexp);
        Matcher matcher = pattern.matcher(line);

        String assign = null;
        if (matcher.find()) {
            assign = matcher.group(0);
        }

        String fieldValue = assign.split("=")[1];

        try {
            DeleteManager.deleteEntry(this.currentDb, targetTable, fieldName, fieldValue);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Preconditions.checkNotNull(line);
    }

    @VisibleForTesting
    void dispatchUpdate(String line) {
        Preconditions.checkNotNull(line);
        String[] tokens = line.split(" ");

        String targetTable = tokens[3];

        String assignmentRegex = "\\w+=\\w+";

        Pattern pattern = Pattern.compile(assignmentRegex);
        Matcher matcher = pattern.matcher(line);

        String assign1 = null;
        if (matcher.find()) {
            assign1 = matcher.group(0);
        }

        String assign2 = null;
        if (matcher.find()) {
            assign2 = matcher.group(0);
        }

        String[] tokens1 = assign1.split("=");
        String[] tokens2 = assign2.split("=");

        String fieldName = tokens1[0];
        String newValue = tokens1[1];
        String oldValue = tokens2[1];

        try {
            UpdateManager.updateEntry(this.currentDb, targetTable, fieldName, oldValue, newValue);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Preconditions.checkNotNull(line);
    }

    @VisibleForTesting
    void dispatchInsert(String line) {
        Preconditions.checkNotNull(line);
        String targetTable = line.split(" ")[2];

        String argsRegexp = "\\(.*?\\)";
        Pattern pattern = Pattern.compile(argsRegexp);
        Matcher matcher = pattern.matcher(line);

        String defArgs = null;
        String insertArgs = null;

        if (matcher.find()) {
            defArgs = matcher.group(0);
        }

        if (matcher.find()) {
            insertArgs = matcher.group(0);
        }

        List<String> bundledInsertArgs = bundleInsertArgs(insertArgs);
        List<String> bundledDefArgs = bundleInsertArgs(defArgs);

        try {
            InsertManager.insertIntoTable(this.currentDb, targetTable, bundledDefArgs, bundledInsertArgs);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Preconditions.checkNotNull(line);
    }

    private List<String> bundleInsertArgs(String insertArgs) {
        Preconditions.checkNotNull(insertArgs);

        insertArgs = insertArgs.substring(1, insertArgs.length() - 1);

        String[] args = insertArgs.split(",");
        List<String> workedArgs = new ArrayList<String>();


        for (String arg : args) {
            workedArgs.add(arg);
        }

        Preconditions.checkNotNull(insertArgs);
        Preconditions.checkNotNull(workedArgs);

        return workedArgs;
    }

    @VisibleForTesting
    void dispatchOpen(String line) {
        Preconditions.checkNotNull(line);
        this.currentDb = line.split(" ")[1] + ".xml";
        System.out.println("Now working on: " + this.currentDb + "!");
        Preconditions.checkNotNull(line);
    }

    @VisibleForTesting
    void dispatchMake(String line) {
        Preconditions.checkNotNull(line);
        String databaseName = line.split(" ")[1];

        try {
            DBCreator.createDataBase(databaseName);
        } catch (DBAlreadyExistsException e) {
            e.printStackTrace();
        }

        Preconditions.checkNotNull(line);
    }

    /**
     * Structure `create table_name(field1, type; field2, type; ...)`
     *
     */

    @VisibleForTesting
    void dispatchCreate(String line) {
        Preconditions.checkNotNull(line);

        String tableNameRegexp = "\\b(?:create)\\s+([a-zA-Z0-9_$#-]*\\.?\\s*(?:[a-zA-Z0-9_]+)*)";
        Pattern pattern = Pattern.compile(tableNameRegexp);
        Matcher matcher = pattern.matcher(line);


        String tableName = null;
        if (matcher.lookingAt()) {
            tableName = matcher.group(1);
        }

        String argsRegexp = "\\(.*\\)";
        pattern = Pattern.compile(argsRegexp);
        matcher = pattern.matcher(line);

        String args = null;

        if (matcher.find()) {
            args = matcher.group(0);
        }

        HashMap argsMap = bundleArgs(args);

        try {
            TableManager.createTable(currentDb, tableName, argsMap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Preconditions.checkNotNull(line);
    }

    private static HashMap<String, String> bundleArgs(String rawArgs) {
        Preconditions.checkNotNull(rawArgs);

        rawArgs = rawArgs.substring(1, rawArgs.length()-1);
        String[] fileds = rawArgs.split(";");

        HashMap<String, String> fieldsMap = new HashMap<>();

        for (String fieldPair : fileds) {
            String[] splited = fieldPair.split(",");
            fieldsMap.put(splited[0].trim(), splited[1].trim());
        }

        Preconditions.checkNotNull(rawArgs);
        Preconditions.checkNotNull(fieldsMap);

        return fieldsMap;
    }
}
