import com.google.common.base.Preconditions;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;


public class DBCreatorTest {

    static final String SAMPLE_DB_NAME_TRUE = "db1";
    static final String SAMPLE_DB_NAME_FALSE = "fals2e";
    static final String SAMPLE_DB_NAME_CREATE = "db1";


    @Test
    public void checkIfDBExistsTrue() {

        assertEquals(DBCreator.checkIfDBExists(SAMPLE_DB_NAME_TRUE), true);
    }

    @Test
    public void checkIfDBExistsFalse() {
        assertEquals(DBCreator.checkIfDBExists(SAMPLE_DB_NAME_FALSE), false);
    }

    @Test(expected = DBAlreadyExistsException.class)
    public void createDBAlreadyExisting() throws DBAlreadyExistsException {
        DBCreator.createDataBase(SAMPLE_DB_NAME_CREATE);
    }

}
