import com.google.common.base.Preconditions;
import org.junit.Assert;
import org.junit.Test;
import java.io.File;
import java.io.IOException;

public class ExportsManagerTest {

    @Test
    public void testGenerateExports() {
        File file = new File("/home/cristi/db_mg_sys/exports/export.csv");
        try {
            ExportsManager.generateExport("db3.xml");
            Assert.assertEquals(file.exists(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
