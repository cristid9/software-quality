import org.easymock.EasyMock;
import org.junit.Test;

public class CommandsDispatcherTest {

    CommandsDispatcher commandsDispatcher = EasyMock.createMock(CommandsDispatcher.class);

    static final String CREATE_DB_TEST_COMMAND = "create table1(nume, string; id, int)\n";
    static final String MAKE_TEST_COMMAND = "make db2";
    static final String OPEN_TEST_COMMAND = "open db2";
    static final String ISNERT_TEST_COMMAND = "insert into table1 (nume, id) values (ana, 1)\n";
    static final String UPDATE_TEST_COMMAND = "update nume=maria from table1 where nume=ana";
    static final String DELETE_TEST_COMMAND = "delete nume from table1 where nume=maria\n";
    static final String SELECT_TEST_COMMAND = "select * from table1\n";
    static final String DROP_TEST_COMMAND = "drop table1\n";
    static final String EXPORT_TEST_COMMAND = "export db\n";
    static final String IMPORT_TEST_COMMAND = "import db\n";

    @Test
    public void testDispatchCommandCreateDB() {

        commandsDispatcher.dispatchCreate(CREATE_DB_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(CREATE_DB_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandMakeCommand() {

        commandsDispatcher.dispatchMake(MAKE_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(MAKE_TEST_COMMAND);
    }


    @Test
    public void testDispatchCommandOpen() {

        commandsDispatcher.dispatchOpen(OPEN_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(OPEN_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandInsert() {

        commandsDispatcher.dispatchOpen(ISNERT_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(ISNERT_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandUpdate() {

        commandsDispatcher.dispatchOpen(UPDATE_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(UPDATE_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandDelete() {

        commandsDispatcher.dispatchOpen(DELETE_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(DELETE_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandSelect() {

        commandsDispatcher.dispatchOpen(SELECT_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(SELECT_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandDrop() {

        commandsDispatcher.dispatchOpen(DROP_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(DROP_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandExport() {

        commandsDispatcher.dispatchOpen(EXPORT_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(EXPORT_TEST_COMMAND);
    }

    @Test
    public void testDispatchCommandImport() {
        commandsDispatcher.dispatchOpen(IMPORT_TEST_COMMAND);
        EasyMock.expectLastCall();
        EasyMock.replay();

        commandsDispatcher.dispatchCommand(IMPORT_TEST_COMMAND);
    }

}