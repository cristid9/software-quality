import com.google.common.base.Preconditions;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SelectManagerTest {

    @Test
    public void testSelectEntries() {

        List<String> expected = new ArrayList<>();
        expected.add("ana,  1, \n");
        try {
            List<String> actual = SelectManager.selectEntries("db2.xml", "table1");

            Assert.assertEquals(actual.size(), expected.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSelectEntriesCheckContent() {

        List<String> expected = new ArrayList<>();
        expected.add("ana,  1, \n");

        try {
            List<String> actual = SelectManager.selectEntries("db2.xml", "table1");

            Assert.assertEquals(actual, expected);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
