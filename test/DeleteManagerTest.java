import org.easymock.EasyMock;
import org.junit.Test;

import java.io.IOException;

import static org.easymock.EasyMock.anyObject;

public class DeleteManagerTest {
    DeleteManager deleteManager = EasyMock.createMock(DeleteManager.class);
    static final String INTERNAL_DELETE_LINE = "sdasad sadas asdasd asdas sadsa sadasd asdas";

    @Test(expected = NullPointerException.class)
    public void deleteEntry() {
        deleteManager.internalDeleteEntry(anyObject(String.class), EasyMock.anyObject(String.class));
        EasyMock.expectLastCall();
        try {
            deleteManager.deleteEntry("db", "table", "fieldName", "value");
        } catch (IOException e) {}
    }

}
