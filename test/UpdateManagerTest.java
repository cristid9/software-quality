import org.easymock.EasyMock;
import org.junit.Test;

import java.io.IOException;

import static org.easymock.EasyMock.anyObject;

public class UpdateManagerTest {
    UpdateManager updateManager = EasyMock.createMock(UpdateManager.class);

    @Test(expected = NullPointerException.class)
    public void testupdateTable() {
        updateManager.internalUpdateEntry(anyObject(String.class), anyObject(String.class), anyObject(String.class));
        EasyMock.expectLastCall();
        try {
            updateManager.updateEntry(anyObject(String.class), anyObject(String.class), anyObject(String.class),
                    anyObject(String.class), anyObject(String.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
