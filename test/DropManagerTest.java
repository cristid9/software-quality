import java.io.File;
import java.io.IOException;

import com.google.common.base.Preconditions;
import org.junit.Assert;
import org.junit.Test;

public class DropManagerTest {

    static final String PATH = "/home/cristi/db_mg_sys/dbs/db3.xml";

    @Test
    public void dropTable() {
        File f = new File(PATH);
        int initialSize = (int) f.length();
        try {
            DropManager.dropTable("db3", "users");
            int newSize = (int) f.length();

            Assert.assertEquals(initialSize, newSize);
        } catch (IOException e) { }
    }
}
