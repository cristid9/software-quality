import org.easymock.EasyMock;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.easymock.EasyMock.anyObject;

public class InsertManagerTest {

    InsertManager insertManager = EasyMock.createMock(InsertManager.class);

    @Test(expected = NullPointerException.class)
    public void insertIntoTable() {
        insertManager.internalInsertTableEntries(anyObject(String.class), anyObject(String.class), anyObject(String.class));
        EasyMock.expectLastCall();
        try {
            insertManager.insertIntoTable(anyObject(String.class), anyObject(String.class), anyObject(List.class), anyObject(List.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
