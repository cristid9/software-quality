import org.easymock.EasyMock;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.easymock.EasyMock.anyObject;

public class TableManagerTest {

    TableManager tableManager = EasyMock.createMock(TableManager.class);

    @Test(expected = NullPointerException.class)
    public void testCreateTable() {
        tableManager.insertNewTable(anyObject(String.class), anyObject(String.class));
        EasyMock.expectLastCall();
        try {
            tableManager.createTable(anyObject(String.class), anyObject(String.class), anyObject(Map.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
